using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Linq;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace spider.Controllers;

[ApiController]
[Route("api/[controller]")]
public class SpiderController : ControllerBase
{
   [HttpPost(Name = "StartSpider")]
   public IActionResult post(String spiderName)
   {
      try
      {
         if (System.IO.File.Exists($"D:\\Zelando\\Spider\\Api\\{spiderName}.bat"))
         {
            var createdOn = System.IO.File.GetCreationTime($"D:\\Zelando\\Spider\\Api\\{spiderName}.bat");
            if (createdOn < DateTime.Now.AddHours(2)) {
               return BadRequest($"Processo ancora in corso per lo stesso distributore {spiderName}");
            }
         }
         System.IO.File.WriteAllText($"D:\\Zelando\\Spider\\Api\\{spiderName}.bat", $"C:\\Users\\Administrator\\AppData\\Local\\Programs\\Python\\Python39\\python.exe D:\\Zelando\\Spider\\Api\\python\\scripts\\logic.py \"{spiderName}\" \"1\"");

         string batDir = "D:\\Zelando\\Spider\\Api\\";
         var proc = System.Diagnostics.Process.Start($"{batDir}\\{spiderName}.bat");
         proc.PriorityClass = ProcessPriorityClass.High;
         // var proc = new Process();
         // proc.StartInfo.WorkingDirectory = batDir;
         // proc.StartInfo.FileName = $"{spiderName}.bat";
         // proc.StartInfo.CreateNoWindow = false;
         // proc.StartInfo.RedirectStandardOutput = true;
         // proc.StartInfo.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
         // proc.Start();
         // proc.PriorityClass = ProcessPriorityClass.High;
         // string output = proc.StandardOutput.ReadToEnd();
         // Console.WriteLine(output);
         // string err = proc.StandardError.ReadToEnd();
         // Console.WriteLine(err);
         // proc.WaitForExit();
         return Ok($"Processo avviato {proc.Id}");
      }
      catch (Exception ex)
      {
         return Ok(ex.Message);
      }
   }

   [HttpGet(Name = "GetXlsx")]
   public IActionResult getXlsxAsync(String SpiderName)
   {
      try
      {
         var stream = new MemoryStream();
         stream.Position = 0;
         String p = new String(@"D:\\Zelando\\Spider\\Api\\python\\scripts\\excelFiles\\" + SpiderName + ".xlsx");
         FileStream fStream = System.IO.File.Open(p, FileMode.Open, FileAccess.Read);
         return File(fStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", SpiderName + ".xlsx");
      }
      catch (Exception ex)
      {
         var stream = new MemoryStream();
         stream.Position = 0;
         Console.WriteLine(ex.Message);
         return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "error.xlsx");
      }

   }
}